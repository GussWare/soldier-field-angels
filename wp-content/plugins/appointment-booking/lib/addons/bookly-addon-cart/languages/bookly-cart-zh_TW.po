msgid ""
msgstr ""
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: POEditor.com\n"
"Project-Id-Version: Bookly Cart (Add-on)\n"
"Language: zh-TW\n"

#: 
msgid "Visible when the chosen time slot has been already booked"
msgstr "所選時間段已經預訂時可見"

#: 
msgid "Date"
msgstr "日期"

#: 
msgid "Time"
msgstr "時間"

#: 
msgid "Price"
msgstr "售價"

#: 
msgid "Edit"
msgstr "編輯"

#: 
msgid "Remove"
msgstr "清除"

#: 
msgid "Total"
msgstr "總計"

#: 
msgid "Deposit"
msgstr "存款"

#: 
msgid "Cart"
msgstr "購物車"

#: 
msgid "If cart is enabled then your clients will be able to book several appointments at once. Please note that WooCommerce integration must be disabled."
msgstr "如果購物車已啟用，然後您的客戶將能夠在一次預定幾個約會。請註意，WooCommerce整合必須被停止。"

#: 
msgid "Columns"
msgstr "列"

#: 
msgid "Reorder"
msgstr "重新排序"

#: 
msgid "Book More"
msgstr "預約更多"

#: 
msgid "Below you can find a list of services selected for booking.\n"
"Click BOOK MORE if you want to add more services."
msgstr "您可以在下方看到預約項目列表.\n"
"如果您想預約更多服務請點擊”繼續添加預約”."

#: 
msgid "Next"
msgstr "下"

#: 
msgid "The highlighted time is not available anymore. Please, choose another time slot."
msgstr "所選時段已滿，請選擇其他的時段。"

#: 
msgid "Settings saved."
msgstr "設定已保存。"

#: 
msgid "To use the cart, disable integration with WooCommerce <a href=\"%s\">here</a>."
msgstr "要使用購物車，停止使用WooCommerce <a href=“%s”>here</a> 。"

#: 
msgid "Tax"
msgstr ""

#: 
msgid "Total tax"
msgstr ""

#: 
msgid "Waiting list"
msgstr ""

#: 
msgid "Subtotal"
msgstr ""

