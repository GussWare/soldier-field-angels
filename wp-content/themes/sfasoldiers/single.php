<?php  
get_header();
?>

 <!-- Start Page Title Section -->
 <div class="page-ttl slider-dos">
    <div class="layer-stretch">
        <div class="page-ttl-container">
            <h1>PRENSA</h1>
        </div>
    </div>
</div><!-- End Page Title Section -->

<?php if(have_posts()): while(have_posts()): the_post(); ?>

<div class="blog">
    <div class="layer-stretch">
        <div class="layer-wrapper pb-20">
            <div class="row">
                <div class="col-lg-12">
                    <div class="blogp pt-4">
                        <div class="blogp-img">
                            <img class="img-fluid" src="uploads/blog-large-1.jpg" alt="">
                        </div>
                        <div class="blogp-title">
                            <h2><?php the_title(  ); ?></h2>
                        </div>
                        <div class="blog-meta blogp-meta">
                            <p><i class="icon-user"></i><span><?php the_author(  ); ?></span></p>
                            <p><i class="icon-clock"></i><span><?php the_date( ); ?></span></p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <?php the_content( ); ?>
                </div>
            </div>
        </div>
    </div>
</div><!-- End Blog Section -->
<?php 
    endwhile;
else: ?>

<div class="blog">
    <?php get_template_part( 'message', 'none' );   ?>
</div>

<?php endif; ?>
    

<?php 
get_footer();
?>