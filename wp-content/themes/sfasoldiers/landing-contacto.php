<!-- Start Header Section -->
<div class="layer-stretch">
    <div class="layer-wrapper pb-3">
    <?php 
    $contacto_titulo = get_field('contacto_titulo', $post_id);
    if($contacto_titulo):
    ?>
        <div class="layer-ttl"><h4><?php echo $contacto_titulo; ?></h4></div>
    <?php endif; ?>
    </div>
</div><!-- End Header Section -->

<?php 
$contacto_mapa = get_field('contacto_mapa', $post_id);
?>
<?php if($contacto_mapa): ?>
<!-- Start Google Map Section -->
<div class="acf-map">
     <div class="marker" data-lat="<?php echo $contacto_mapa['lat']; ?>" data-lng="<?php echo $contacto_mapa['lng']; ?>"></div>
 </div>
<!-- End Google Map Section -->
<?php endif; ?>


<!-- Start Contact Section -->
<div class="contact-block">
    <div class="row">
        <div class="col-md-4">
            <div class="block">
                <i class="icon-phone"></i>
                <?php 
                $contacto_telefono_titulo = get_field('contacto_telefono_titulo', $post_id);
                if($contacto_telefono_titulo):
                ?>
                    <span><?php echo $contacto_telefono_titulo; ?></span>
                <?php 
                endif;
                ?>

                <?php 
                $contacto_telefonos = get_field('contacto_telefonos', $post_id);
                if($contacto_telefonos):
                    foreach($contacto_telefonos AS $telefono):
                ?>
                        <p><?php echo $telefono['telefono']; ?></p>
                <?php 
                    endforeach;
                endif; ?>
            </div>
        </div>
        <div class="col-md-4">
            <div class="block">
                <i class="icon-envelope-letter"></i>
                <?php 
                $contacto_correo_titulo = get_field('contacto_correo_titulo', $post_id);
                if($contacto_correo_titulo):
                ?>
                    <span><?php echo $contacto_correo_titulo; ?></span>
                <?php 
                endif;
                ?>
                
                <?php 
                $contacto_correos = get_field('contacto_correos', $post_id);
                if($contacto_correos):
                    foreach($contacto_correos AS $correos):
                ?>
                        <p><?php echo $correos['correo']; ?></p>
                <?php 
                    endforeach;
                endif; ?>
            </div>
        </div>
        <div class="col-md-4">
            <div class="block">
                <i class="icon-location-pin"></i>

                <?php 
                $contacto_direccion_titulo = get_field('contacto_direccion_titulo', $post_id);
                if($contacto_direccion_titulo):
                ?>
                    <span><?php echo $contacto_direccion_titulo; ?></span>
                <?php 
                endif;
                ?>

                <?php 
                $contacto_direccion = get_field('contacto_direccion', $post_id);
                if($contacto_direccion):
                ?>
                    <p><?php echo $contacto_direccion; ?></p>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div><!-- End Contact Section -->


<!-- Start Contact Form Section -->
<div class="layer-tablet">
    <div class="layer-wrapper">
        
        <?php 
        $contacto_titulo_formulario = get_field('contacto_titulo_formulario', $post_id);
        if($contacto_titulo_formulario):
        ?>
            <div class="layer-ttl"><h4><?php echo $contacto_titulo_formulario; ?></h4></div>
        <?php endif; ?>

        <?php 
        $contacto_formulario_descripcion = get_field('contacto_formulario_descripcion', $post_id);
        if($contacto_formulario_descripcion):
        ?>
            <div class="layer-sub-ttl"><?php echo $contacto_formulario_descripcion; ?></div>
        <?php endif; ?>

        <div class="contact-error"></div>
        <?php echo do_shortcode('[contact-form-7 id="4"]') ?>
        
    </div>
</div><!-- End Contact Form Section -->