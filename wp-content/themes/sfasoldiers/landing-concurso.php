<div class="layer-stretch">
    <div class="layer-wrapper">
        <div class="layer-wrapper text-center">
            <?php 
            $concurso_titulo = get_field('concurso_titulo', $post_id);
            if($concurso_titulo):
            ?>
                <div class="layer-ttl"><h4><?php echo $concurso_titulo; ?></h4></div>
            <?php endif; ?>

            <?php 
            $concurso_descripcion = get_field('concurso_descripcion', $post_id);
            if($concurso_descripcion): 
            ?>
                <div class="action-content"><?php echo $concurso_descripcion; ?></div>
            <?php endif; ?>

            <?php 
            $concurso_link = get_field('concurso_link', $post_id);
            $concurso_link_desc = get_field('concurso_link_desc', $post_id);
            if($concurso_link && $concurso_link_desc):
            ?>
                <a href="<?php echo $concurso_link ?>" class="btn btn-outline btn-primary btn-pill btn-outline-2x btn-lg mt-3"><?php echo $concurso_link_desc; ?></a>
            <?php 
            endif;
            ?>

        </div>
    </div>
</div>