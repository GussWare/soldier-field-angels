<?php
/**
 * Template Name: LandingPage
 * Description: Template tipo landing page
 */

get_header();

$post_id = get_the_ID();
?>

<!-- Start Slider Section -->
<div id="slider" class="slider-dark">
    <?php include( locate_template('landing-slider.php') ) ?>
</div>
<!-- End Slider Section -->

<!-- Start Acerca de Section -->
<div id="nosotros" class="service">
    <?php include( locate_template('landing-acercade.php') ); ?>
</div><!-- End of Acerca de Section -->

<!-- Start Equipo Section -->
<div id="equipo" class="team">
    <?php include( locate_template('landing-equipo.php') ); ?>
</div><!-- End of Equipo de Section -->

<!-- Start Portafolio Section -->
<div id="portafolio" class="portafolio">
    <?php include( locate_template('landing-portafolio.php') ); ?>
</div><!-- End of Portafolio de Section -->

<!-- Start Prensa Section -->
<div id="prensa" class="blog">
    <?php include( locate_template('landing-prensa.php') ); ?>
</div><!-- End of Prensa de Section -->
 

<!-- Start concurso Section -->
<div id="concurso" class="action">
    <?php include( locate_template('landing-concurso.php') ); ?>
</div><!-- End of concurso de Section -->

<!-- Start alianzas Section -->
<div id="alianzas" class="alianzas">
    <?php include( locate_template('landing-alianzas.php') ); ?>
</div><!-- End of alianzas de Section -->

<!-- Start alianzas Section -->
<div id="contacto" class="contacto">
    <?php include( locate_template('landing-contacto.php') ); ?>
</div><!-- End of alianzas de Section -->



<?php get_footer(); ?>