<?php 
get_header();
?>

<div class="blog">
            <div class="layer-stretch">
                <div class="layer-wrapper pb-20">
                    <div class="row">
                        <div class="col-lg-8">
                            <div class="blogp pt-4">
                                <div class="blogp-img">
                                    <img class="img-fluid" src="uploads/blog-large-1.jpg" alt="">
                                </div>
                                <div class="blogp-title">
                                    <h2>John Doe Creative Work</h2>
                                </div>
                                <div class="blog-meta blogp-meta">
                                    <p><i class="icon-user"></i><span>Admin</span></p>
                                    <p><i class="icon-clock"></i><span>24 Jul</span></p>
                                    <p><i class="icon-bubble"></i><span>29</span></p>
                                </div>
                                <div class="blogp-post">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quibusdam odio architecto consectetur excepturi consequatur eligendi sequi est dolorem molestias eaque animi numquam quidem aspernatur, debitis magnam nostrum amet dignissimos deleniti!</p>
                                    <p>Minima ratione modi amet in aspernatur voluptate necessitatibus exercitationem deserunt blanditiis. Id maiores dignissimos quae facilis est eaque non dolores nulla, tempora ipsum soluta veniam, ducimus aut reiciendis magni quos.</p>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <ul class="list-icon">
                                                <li><i class="fa fa-hand-o-right"></i>Fully Responsive</li>
                                                <li><i class="fa fa-hand-o-right"></i>SEO Friendly</li>
                                                <li><i class="fa fa-hand-o-right"></i>Clean Code</li>
                                                <li><i class="fa fa-hand-o-right"></i>Fast Load</li>
                                            </ul>
                                        </div>
                                        <div class="col-sm-4">
                                            <ul class="list-icon">
                                                <li><i class="fa fa-hand-o-right"></i>Content Marketing</li>
                                                <li><i class="fa fa-hand-o-right"></i>Easy to Customize</li>
                                                <li><i class="fa fa-hand-o-right"></i>Google Web Fonts</li>
                                                <li><i class="fa fa-hand-o-right"></i>Font Icons</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="row mt-3 mb-5">
                                        <div class="col-md-6">
                                            <div class="img-hover-icon">
                                                <img class="img-fluid" src="uploads/blog-2.jpg" alt=""/>
                                                <div class="img-icon-wrapper">
                                                    <div class="img-icon">
                                                        <a href="uploads/blog-2.jpg" class="gallery"><i class="icon-picture"></i></a>
                                                        <a href="#" class="animated zoomIn"><i class="icon-link"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="img-hover-icon">
                                                <img class="img-fluid" src="uploads/blog-3.jpg" alt=""/>
                                                <div class="img-icon-wrapper img-icon-colored">
                                                    <div class="img-icon">
                                                        <a href="uploads/blog-3.jpg" class="gallery"><i class="icon-picture"></i></a>
                                                        <a href="#" class="animated zoomIn"><i class="icon-link"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <p>Libero natus laborum, beatae alias facilis, quod asperiores nulla unde. Eligendi aspernatur, consequuntur beatae magnam deleniti quaerat alias aperiam, velit voluptate dignissimos laudantium cupiditate sunt et explicabo non. Iure, consequuntur.</p>
                                    <p>Impedit, earum. Ipsum eligendi officiis, voluptate. Sunt, illum. Soluta, odit illum cupiditate consequatur eos delectus assumenda. Voluptate iste natus, dolorum, eligendi iure, nulla repellendus distinctio rem quam voluptatum, blanditiis est.</p>
                                    <blockquote class="blockquote">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab omnis ipsa, quis beatae ex nam excepturi quae sequi molestias dolorem deserunt voluptas corrupti fugit eum totam nobis dicta officiis mollitia.</p>
                                        <footer class="blockquote-footer">Someone famous in <cite title="Source Title">Source Title</cite></footer>
                                    </blockquote>
                                </div>
                                <div class="blogp-footer">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <ul class="">
                                                <li><a href="#" class="badge badge-light">News</a></li>
                                                <li><a href="#" class="badge badge-light">History</a></li>
                                                <li><a href="#" class="badge badge-light">Science</a></li>
                                                <li><a href="#" class="badge badge-light">Tech</a></li>
                                                <li><a href="#" class="badge badge-light">Web</a></li>
                                            </ul>
                                        </div>
                                        <div class="col-md-6 text-right font-16">
                                            <ul class="">
                                                <li><a href="#"><i class="icon-social-facebook"></i></a></li>
                                                <li><a href="#"><i class="icon-social-twitter"></i></a></li>
                                                <li><a href="#"><i class="icon-social-linkedin"></i></a></li>
                                                <li><a href="#"><i class="icon-social-instagram"></i></a></li>
                                                <li><a href="#"><i class="icon-social-google"></i></a></li>
                                                <li><a href="#"><i class="icon-social-pinterest"></i></a></li>
                                                <li><a href="#"><i class="icon-social-tumblr"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="blogp-author">
                                    <div class="sub-ttl"><div class="sub-ttl-text">Author Info</div></div>
                                    <div class="row">
                                        <div class="col-3">
                                            <div class="img-hover-icon">
                                                <img class="img-fluid" src="uploads/author-1.jpg" alt="">
                                                <div class="img-icon-wrapper">
                                                    <div class="img-icon">
                                                        <a href="#"><i class="icon-link"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-9 blog-author-details">
                                            <h4><a href="#">Daniel Barnes</a></h4>
                                            <a>HOD Orthlogy Department</a>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sequi quaerat quasi eum tempora illum.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="comment">
                                    <div class="sub-ttl"><div class="sub-ttl-text">Comments(10)</div></div>
                                    <ul class="comment-list">
                                        <li>
                                            <div class="row">
                                                <div class="col-2 hidden-xs-down pr-0 comment-img">
                                                    <img src="uploads/comment-1.jpg" alt="">
                                                </div>
                                                <div class="col-10 comment-detail text-left">
                                                    <div class="comment-meta">
                                                        <span>Jorah Mormant</span>
                                                        <span>27 June 2017</span>
                                                    </div>
                                                    <div class="comment-post">
                                                        Laboris nisi ut aliquip ex ea Duis aute irure dolor in reprehenderit in voluptate velit .Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit .
                                                    </div>
                                                    <ul class="comment-action">
                                                        <li><a><i class="fa fa-thumbs-up"></i>Like</a></li>
                                                        <li><a><i class="fa fa-thumbs-down"></i>Dislike</a></li>
                                                        <li><a><i class="fa fa-reply"></i>Reply</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <ul class="comment-list second-comment">
                                                <li>
                                                    <div class="row">
                                                        <div class="col-2 hidden-xs-down pr-0 comment-img">
                                                            <img src="uploads/comment-6.jpg" alt="">
                                                        </div>
                                                        <div class="col-10 comment-detail text-left">
                                                            <div class="comment-meta">
                                                                <span>Anthony Collins</span>
                                                                <span>27 June 2017</span>
                                                            </div>
                                                            <div class="comment-post">
                                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illo ad odio, doloribus dicta. Nesciunt odit, sed optio repudiandae.
                                                            </div>
                                                            <ul class="comment-action">
                                                                <li><a><i class="fa fa-thumbs-up"></i>Like</a></li>
                                                                <li><a><i class="fa fa-thumbs-down"></i>Dislike</a></li>
                                                                <li><a><i class="fa fa-reply"></i>Reply</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </li>
                                        <li>
                                            <div class="row">
                                                <div class="col-2 hidden-xs-down pr-0 comment-img">
                                                    <img src="uploads/comment-2.jpg" alt="">
                                                </div>
                                                <div class="col-10 comment-detail text-left">
                                                    <div class="comment-meta">
                                                        <span>Micheal Doe</span>
                                                        <span>27 June 2017</span>
                                                    </div>
                                                    <div class="comment-post">
                                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illo ad odio, doloribus dicta. Nesciunt odit, sed optio repudiandae.
                                                    </div>
                                                    <ul class="comment-action">
                                                        <li><a><i class="fa fa-thumbs-up"></i>Like</a></li>
                                                        <li><a><i class="fa fa-thumbs-down"></i>Dislike</a></li>
                                                        <li><a><i class="fa fa-reply"></i>Reply</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="row">
                                                <div class="col-2 hidden-xs-down pr-0 comment-img">
                                                    <img src="uploads/comment-3.jpg" alt="">
                                                </div>
                                                <div class="col-10 comment-detail text-left">
                                                    <div class="comment-meta">
                                                        <span>Gerry George</span>
                                                        <span>27 June 2017</span>
                                                    </div>
                                                    <div class="comment-post">
                                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illo ad odio, doloribus dicta. Nesciunt odit, sed optio repudiandae.
                                                    </div>
                                                    <ul class="comment-action">
                                                        <li><a><i class="fa fa-thumbs-up"></i>Like</a></li>
                                                        <li><a><i class="fa fa-thumbs-down"></i>Dislike</a></li>
                                                        <li><a><i class="fa fa-reply"></i>Reply</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="row">
                                                <div class="col-2 hidden-xs-down pr-0 comment-img">
                                                    <img src="uploads/comment-4.jpg" alt="">
                                                </div>
                                                <div class="col-10 comment-detail text-left">
                                                    <div class="comment-meta">
                                                        <span>Samuel Wikensy</span>
                                                        <span>27 June 2017</span>
                                                    </div>
                                                    <div class="comment-post">
                                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illo ad odio, doloribus dicta. Nesciunt odit, sed optio repudiandae.
                                                    </div>
                                                    <ul class="comment-action">
                                                        <li><a><i class="fa fa-thumbs-up"></i>Like</a></li>
                                                        <li><a><i class="fa fa-thumbs-down"></i>Dislike</a></li>
                                                        <li><a><i class="fa fa-reply"></i>Reply</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="row">
                                                <div class="col-2 hidden-xs-down pr-0 comment-img">
                                                    <img src="uploads/comment-5.jpg" alt="">
                                                </div>
                                                <div class="col-10 comment-detail text-left">
                                                    <div class="comment-meta">
                                                        <span>Meera Reed</span>
                                                        <span>27 June 2017</span>
                                                    </div>
                                                    <div class="comment-post">
                                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illo ad odio, doloribus dicta. Nesciunt odit, sed optio repudiandae.
                                                    </div>
                                                    <ul class="comment-action">
                                                        <li><a><i class="fa fa-thumbs-up"></i>Like</a></li>
                                                        <li><a><i class="fa fa-thumbs-down"></i>Dislike</a></li>
                                                        <li><a><i class="fa fa-reply"></i>Reply</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <ul class="comment-list second-comment">
                                                <li>
                                                    <div class="row">
                                                        <div class="col-2 hidden-xs-down pr-0 comment-img">
                                                            <img src="uploads/comment-6.jpg" alt="">
                                                        </div>
                                                        <div class="col-10 comment-detail text-left">
                                                            <div class="comment-meta">
                                                                <span>Anthony Collins</span>
                                                                <span>27 June 2017</span>
                                                            </div>
                                                            <div class="comment-post">
                                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illo ad odio, doloribus dicta. Nesciunt odit, sed optio repudiandae.
                                                            </div>
                                                            <ul class="comment-action">
                                                                <li><a><i class="fa fa-thumbs-up"></i>Like</a></li>
                                                                <li><a><i class="fa fa-thumbs-down"></i>Dislike</a></li>
                                                                <li><a><i class="fa fa-reply"></i>Reply</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                                <div class="comment-form">
                                    <div class="sub-ttl"><div class="sub-ttl-text">Leave A Reply</div></div>
                                    <div class="row pt-4">
                                        <div class="col-sm-6">
                                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label form-input">
                                                <input class="mdl-textfield__input" type="text" pattern="[A-Z,a-z, ]*" id="comment-name">
                                                <label class="mdl-textfield__label" for="comment-name">Name <em> *</em></label>
                                                <span class="mdl-textfield__error">Please Enter Valid Name!</span>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label form-input">
                                                <input class="mdl-textfield__input" type="text" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" id="comment-email">
                                                <label class="mdl-textfield__label" for="comment-email">Email <em> *</em></label>
                                                <span class="mdl-textfield__error">Please Enter Valid Email!</span>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="mdl-textfield mdl-js-textfield form-input is-upgraded">
                                                <textarea class="mdl-textfield__input" rows="4" id="comment-message"></textarea>
                                                <label class="mdl-textfield__label" for="comment-message">Your Comment ...</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 text-center">
                                            <div class="form-submit">
                                                <button type="submit" class="mdl-button mdl-js-button mdl-js-ripple-effect button button-primary">Submit Comment<span class="mdl-button__ripple-container"><span class="mdl-ripple"></span></span></button>
                                            </div>
                                        </div>  
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 pt-4">
                            <div class="sidebar">
                                <div class="sub-ttl"><h6 class="sub-ttl-text">Search</h6></div>
                                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label form-input">
                                    <input type="text" id="search" class="mdl-textfield__input">
                                    <label for="search" class="mdl-textfield__label">Search your Keyword</label>
                                </div>
                            </div>
                            <div class="sidebar">
                                <div class="sub-ttl"><h6 class="sub-ttl-text">Recent Post</h6></div>
                                <a href="#" class="row blog-recent">
                                    <div class="col-4 blog-recent-img pr-0">
                                        <img class="img-thumbnail" src="uploads/recent-1.jpg" alt="">
                                    </div>
                                    <div class="col-8 blog-recent-post">
                                        <h4>Why Food Poisoning happened and How To</h4>
                                        <p>08 Jun 2017</p>
                                    </div>
                                </a>
                                <a href="#" class="row blog-recent">
                                    <div class="col-4 blog-recent-img pr-0">
                                        <img class="img-thumbnail" src="uploads/recent-2.jpg" alt="">
                                    </div>
                                    <div class="col-8 blog-recent-post">
                                        <h4>Which Healthy Food Fads Should You Follow?</h4>
                                        <p>27 Apr 2017</p>
                                    </div>
                                </a>
                                <a href="#" class="row blog-recent">
                                    <div class="col-4 blog-recent-img pr-0">
                                        <img class="img-thumbnail" src="uploads/recent-3.jpg" alt="">
                                    </div>
                                    <div class="col-8 blog-recent-post">
                                        <h4>A Broken Heart Can Hurt More Than You Think</h4>
                                        <p>29 Jan 2017</p>
                                    </div>
                                </a>
                                <a href="#" class="row blog-recent">
                                    <div class="col-4 blog-recent-img pr-0">
                                        <img class="img-thumbnail" src="uploads/recent-4.jpg" alt="">
                                    </div>
                                    <div class="col-8 blog-recent-post">
                                        <h4>Keep it Clean: Make Sure Your Fruits and Veggies</h4>
                                        <p>24 Jan 2017</p>
                                    </div>
                                </a>
                                <a href="#" class="row blog-recent">
                                    <div class="col-4 blog-recent-img pr-0">
                                        <img class="img-thumbnail" src="uploads/recent-5.jpg" alt="">
                                    </div>
                                    <div class="col-8 blog-recent-post">
                                        <h4>Should I bring my child in for a routine physical?</h4>
                                        <p>15 Jan 2017</p>
                                    </div>
                                </a>
                            </div>
                            <div class="sidebar">
                                <div class="sub-ttl"><h6 class="sub-ttl-text">Category</h6></div>
                                <ul class="category-list">
                                    <li><a href="#"><i class="fa fa-newspaper-o"></i>News</a><span>(10)</span></li>
                                    <li><a href="#"><i class="fa fa-history"></i>History</a><span>(20)</span></li>
                                    <li><a href="#"><i class="fa fa-th"></i>Mythology</a><span>(9)</span></li>
                                    <li><a href="#"><i class="fa fa-cloud"></i>Technology</a><span>(21)</span></li>
                                    <li><a href="#"><i class="fa fa-flask"></i>Science</a><span>(13)</span></li>
                                    <li><a href="#"><i class="fa fa-info-circle"></i>New Disese</a><span>(7)</span></li>
                                    <li><a href="#"><i class="fa fa-hospital-o"></i>Health</a><span>(5)</span></li>
                                    <li><a href="#"><i class="fa fa-wheelchair-alt"></i>wellness</a><span>(8)</span></li>
                                </ul>
                            </div>
                            <div class="sidebar">
                                <div class="sub-ttl"><div class="sub-ttl-text">Popular Tags</div></div>
                                <div class="tag-list">
                                    <a href="#" class="">News</a>
                                    <a href="#" class="">HTML5</a>
                                    <a href="#" class="tag-colored">CSS3</a>
                                    <a href="#" class="">Corporate</a>
                                    <a href="#" class="">Business</a>
                                </div>
                            </div>
                            <div class="sidebar">
                                <div class="sub-ttl"><div class="sub-ttl-text">Popular Tags</div></div>
                                <div class="tag-list tag-list-2">
                                    <a href="#" class="">Tag</a>
                                    <a href="#" class="">Tech</a>
                                    <a href="#" class="tag-colored">Science</a>
                                    <a href="#" class="">Doctor</a>
                                    <a href="#" class="tag-colored">Clinic</a>
                                </div>
                            </div>
                            <div class="sidebar">
                                <div class="sub-ttl"><div class="sub-ttl-text">Popular Tags</div></div>
                                <div class="">
                                    <a href="#" class="badge badge-primary m-1">News</a>
                                    <a href="#" class="badge badge-success m-1">Tech</a>
                                    <a href="#" class="badge badge-danger m-1">Science</a>
                                    <a href="#" class="badge badge-warning m-1">Doctor</a>
                                    <a href="#" class="badge badge-secondary m-1">Latest</a>
                                    <a href="#" class="badge badge-dark m-1">Coding</a>
                                    <a href="#" class="badge badge-info m-1">Web</a>
                                    <a href="#" class="badge badge-light m-1">App</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div><!-- End Blog Section -->