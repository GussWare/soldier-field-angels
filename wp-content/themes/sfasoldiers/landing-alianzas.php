<div class="layer-stretch">
    <div class="layer-wrapper pb-20">

        <?php 
        $alianza_titulo = get_field('alianza_titulo', $post_id); 
        if($alianza_titulo): 
        ?>

            <div class="layer-ttl"><h4><?php echo $alianza_titulo; ?></h4></div>

        <?php endif; ?>

        <?php 
        $alianzas_descripcion = get_field('alianzas_descripcion', $post_id);
        if($alianzas_descripcion):
        ?>
            <div class="layer-sub-ttl"><?php echo $alianzas_descripcion; ?></div>
        <?php endif; ?>

        <div class="row pt-4">

             <div class="col-12">
                <div class="owl-carousel owl-theme multi-item-slider">
                    <?php 
                        $alianzas = get_field('alianzas_imagen', $post_id);
                        if(count($alianzas) > 0):
                            foreach($alianzas AS $alianza):
                        ?>
                                <div class="theme-owlslider-container">
                                    <img class="img-responsive" src="<?php echo $alianza['alianza_imagen']; ?>" alt="">
                                </div>
                        <?php 
                            endforeach;
                        endif; 
                    ?>
                </div>
            </div>
        </div>
    </div> 
</div>