<div class="flexslider slider-wrapper">
    <div id="element">
        <?php 
        $imagen_de_fondo_slider = get_field('imagen_de_fondo_slider');
        if($imagen_de_fondo_slider): 
            foreach($imagen_de_fondo_slider AS $imagen_slider):
        ?>
            <img src="<?php echo $imagen_slider['imagen']; ?>" >
        <?php 
            endforeach;
       endif; 
    ?>
    </div>

    <ul class="slides">
        <?php 
        $slider = get_field('loopslider');
        if($slider): 
            foreach($slider AS $slider_value):
        ?>

            <div class="layer-stretch">
                    <div class="slider-info">
                        <h1><?php echo $slider_value['titulo']; ?></h1>
                        <p class="animated fadeInDown"><?php echo $slider_value['parrafo']; ?></p>

                        <?php if(count($slider_value['link']) > 0): ?>
                        <div class="slider-button">
                            <a class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored mdl-js-ripple-effect button button-primary button-pill"><?php echo $slider_value['link_texto']; ?></a>
                        </div>
                        <?php endif; ?>
                    </div>
                </div>

            <?php 
            endforeach;
        endif; 
        ?>
    </ul>
</div>
        