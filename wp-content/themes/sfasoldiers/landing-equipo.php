<div class="layer-stretch">
    <div class="layer-wrapper pb-20">

        <?php 
        $equipo_titulo = get_field('equipo_titulo');
        if($equipo_titulo): 
        ?>
            <div class="layer-ttl"><h4><?php echo $equipo_titulo; ?></h4></div>
        <?php endif; ?>

        <?php 
        $equipo_parrafo = get_field('equipo_parrafo');
        if($equipo_parrafo):
        ?>
        <div class="layer-sub-ttl"><?php echo $equipo_parrafo; ?></div>
        <?php endif; ?>

        <div class="row pt-4">

            <?php 

            $args = array(
                'post_type' => 'cpt_equipo',
                'orderby' => 'post_date',
                'order' => 'ASC',
                'posts_per_page' => EQUIPO_LIMIT_SHOW,
            );

            $wp_query = new WP_Query( $args );

            if($wp_query->have_posts()) :

                while($wp_query->have_posts()): $wp_query->the_post();
?>
                    <div class="col-sm-6 col-md-6 col-lg-3">
                        <div class="team-block">
                            <div class="team-img">
                                <img src="<?php echo get_field('fotografia', get_the_ID()); ?>" alt="">
                                <div class="team-description">
                                    <?php echo get_field('descripcion', get_the_ID()); ?>
                                </div>
                            </div>
                            <div class="team-details">
                                <h3><?php echo get_field('nombre_integrante', get_the_ID()); ?></h3>
                                <p><?php echo get_field('puesto', get_the_ID()); ?></p>
                            </div>
                            <div class="team-social">
                                <?php 
                                $redes = get_field('redes_sociales', get_the_ID());
                                if($redes):
                                ?>
                                    <ul>
                                        <?php foreach($redes as $red): ?>
                                            <li><a href="<?php echo $red['link']; ?>"><i class="<?php echo $red['icono']; ?>"></i></a></li>
                                        <?php endforeach; ?>
                                    </ul>
                                <?php 
                                endif;
                                ?>
                            </div>
                        </div>
                    </div>
<?php
                endwhile; wp_reset_query();
            endif;
?>
        </div>
        <div class="row text-center">
            <a href="<?php echo get_site_url() . '/nuestro-equipo/'; ?>" class="btn btn-primary btn-vermas">Leer más</a>
        </div>
    </div> 
</div>