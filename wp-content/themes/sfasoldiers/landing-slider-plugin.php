<div class="container">
      <div id="element" style="width:800px;">
        <?php 
        $slider_background = get_field('imagen_de_fondo');
        if($slider_background): 
        ?>
            <img src="<?php echo $slider_background; ?>" >
        <?php endif; ?>
      </div>
</div>


<div class="flexslider slider-wrapper">
    <ul class="slides">
    <?php 
    $slider = get_field('loopslider');
    if($slider): 
        foreach($slider AS $slider_value):
    ?>
        <li>
            <div class="slider-backgroung-image" style="
            <?php 
            $slider_background = get_field('imagen_de_fondo');
            if($slider_background): 
            ?>
            background-image: url(<?php echo $slider_background; ?>);
            <?php endif; ?>
            ">
                <div class="layer-stretch">
                    <div class="slider-info">
                        <h1><?php echo $slider_value['titulo']; ?></h1>
                        <p class="animated fadeInDown"><?php echo $slider_value['parrafo']; ?></p>

                        <?php if(count($slider_value['link']) > 0): ?>
                        <div class="slider-button">
                            <a class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored mdl-js-ripple-effect button button-primary button-pill"><?php echo $slider_value['link_texto']; ?></a>
                        </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </li> 
    <?php 
        endforeach;
    endif; 
    ?>
    </ul>
</div>
        