<?php

if (!function_exists('theme_setup_init')) {

    /**
     * Funcion que se encarga de inicializar la funcionalidad para el tema
     *
     * @return void
     */
    function theme_setup_init()
    {
        // agregamos soporte para miniaturas en las entradas del blog
        add_theme_support('post-thumbnails');

        // agregamos soporte para menu principal
        register_nav_menu('home_principal', __('Principal Home', 'landing'));

        // agregamos menu secundario para las demas paginas
        register_nav_menu('paginas_menu', __('Paginas Menu', 'landing'));
    }

}

add_action('after_setup_theme', 'theme_setup_init');

// GLOBAL THEME OPTIONS
if (function_exists('acf_add_options_page')) {

    // acf_add_options_page('More Theme Settings');

    acf_add_options_page(array(
        'menu_title' => 'Opciones de la página',
        'page_title' => 'Opciones de la página',
        'menu_slug' => 'opciones-pagina',
        'capability' => 'edit_posts',
        'redirect' => false,
    ));

}

/* Google maps javascript API key integration for ACF */
//https://developers.google.com/maps/documentation/javascript/get-api-key
if (!function_exists('my_map_acf_init')) {

    function my_map_acf_init()
    {
        acf_update_setting('google_api_key', GOOGLE_API_KEY_MAPS);
    }

    add_action('acf/init', 'my_map_acf_init');
}

//ACF Google Map
if (!function_exists('adn_acf_gmap')) {

    function adn_acf_gmap()
    {
        $template_url = get_bloginfo('template_url');

        wp_enqueue_script('acf-gmap', '//maps.googleapis.com/maps/api/js?key=' . GOOGLE_API_KEY_MAPS, array('jquery'), '1.10.2', 1);
        wp_enqueue_script('config-acf-gmap', $template_url . '/assets/js/acf-gmap.js', array('jquery', 'acf-gmap'), '', 1);
    }

    add_action('wp_head', 'adn_acf_gmap');
}
