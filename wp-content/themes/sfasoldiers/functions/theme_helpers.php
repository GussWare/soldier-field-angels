<?php

if (!function_exists('helper_custom_excerpt')) {

    /**
     * Funcion que se encarga de mostrar el texto de acuerdo al limite de caracteres
     * que se definen en las constantes
     *
     * @return string
     */
    function helper_custom_excerpt()
    {
        return wp_trim_words(get_the_excerpt(), EXCERT_LIMIT_SHOW, ' ...');
    }
}
