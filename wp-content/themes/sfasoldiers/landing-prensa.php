
<div class="layer-stretch">
    <div class="layer-wrapper pb-3">
        
    <?php 
        $prensa_titulo = get_field('prensa_titulo');
        if($prensa_titulo): 
        ?>
            <div class="layer-ttl"><h4><?php echo $prensa_titulo; ?></h4></div>
        <?php
         endif; 
        ?>

        <?php 
        $prensa_subtitulo = get_field('prensa_subtitulo');
        if($prensa_subtitulo):
        ?>
            <div class="layer-sub-ttl"><?php echo $prensa_subtitulo; ?></div>
        <?php endif; ?>


        <?php
			query_posts(array(
				"showposts" => 3
			));
			
            if(have_posts()):
        ?>
                <div class="row pt-4">
                    <?php 
                        while(have_posts()): the_post();
                    
                            get_template_part('content', get_post_format());

                        endwhile;
                    ?>
                </div>

                <div class="row text-center">
                        <a href="<?php echo get_site_url( ); ?>/prensa/" class="btn btn-primary btn-vermas">Leer más</a>
                </div>
        <?php 
        else:

            get_template_part('message', 'none');

        endif;
        ?>
    </div>
</div>
