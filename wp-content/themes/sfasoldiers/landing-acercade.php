<div class="layer-stretch">
    <div class="layer-wrapper pb-20">

        <?php 
         $acercade_titulo = get_field('acercade_titulo'); 
         if($acercade_titulo):
        ?>
        <div class="layer-ttl">
            <h4>
                <?php  echo $acercade_titulo; ?>
            </h4>
        </div>
        <?php  endif; ?>

         <?php 
            $acercade_subtitulo = get_field('acercade_subtitulo');
            if($acercade_subtitulo):
            ?>
            <p class="layer-sub-ttl">
                <?php echo $acercade_subtitulo; ?>
            </p>
        <?php endif; ?>


        <?php 
            $acercade_servicios = get_field('acercade_servicios');
            if($acercade_servicios):
            ?>
                <div class="row pt-4">
                    
                    <?php 
                        foreach($acercade_servicios AS $servicio):
                    ?>

                        <div class="col-sm-6 col-md-4">
                            <div class="service-card-1">
                                <div class="service-icon">
                                    <img src="<?php echo $servicio['imagen']; ?>" />
                                </div>
                                <div class="service-heading"><?php echo $servicio['texto']; ?></div>
                            </div>
                        </div>

                    <?php  
                        endforeach;
                    ?>
                </div>
        <?php  
             endif;
        ?>
    </div>
</div>
