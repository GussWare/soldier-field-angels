<?php
/**
 * Template for displaying search forms in site
 *
 */

?>

<?php $unique_id = esc_attr( uniqid( 'search-form-' ) ); ?>

<form method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
  <div class="form-group row">
    <div class="col-6" style="margin: 0 auto;">
      <input type="search" class="form-control" id="<?php echo $unique_id; ?>" placeholder="Buscar ...">
    </div>
  </div>
  
  <div class="form-group row">
    <div class="col-12">
      <button type="submit" class="btn btn-danger">Ir a Inicio</button>
      <button type="submit" class="btn btn-primary">Buscar</button>
    </div>
  </div>
</form>