<!-- Start Portfolio Section -->
<div class="layer-stretch">
    <div class="layer-wrapper pb-20">
        <?php 
        $portafolio_titulo = get_field('portafolio_titulo');
        if($portafolio_titulo): 
        ?>
            <div class="layer-ttl"><h4><?php echo $portafolio_titulo; ?></h4></div>
        <?php endif; ?>

        <?php 
        $portafolio_parrafo = get_field('portafolio_parrafo');
        if($portafolio_parrafo):
        ?>
            <div class="layer-sub-ttl"><?php echo $portafolio_parrafo; ?></div>
        <?php endif; ?>

        <?php 
        $portafolio_botones = get_field('portafolio_botones');
        if($portafolio_botones):
            ?>
             <div class="portfolio-header text-center pt-4">
            <?php
            $i = 0;
            foreach($portafolio_botones AS $boton):
        ?>
            <button class="portfolio-filter <?php if($i == 0): echo 'active'; endif; ?>" data-filter="<?php echo $boton['boton_key']; ?>"><?php echo $boton['boton_value']; ?></button>
        <?php 
            $i++;
            endforeach;
            ?>
            </div>
            <?php
        endif;
        ?>

        <?php 
        $portafolios = get_field('portafolios');
        if($portafolios):
            $i = 0;

        ?>

            <div class="portfolio-wrapper">
                    <ul class="row">
        <?php
            foreach($portafolios AS $portafolio):
        ?>
                        <li class="col-md-6 col-lg-3 portfolio-img filter <?php echo $portafolio['portafolios_keys']; ?>">
                        <a target="_blank" href="
                                <?php if($portafolio['portafolios_link']): echo $portafolio['portafolios_link']; endif; ?>
                                ">
                            <figure class="effect-zoe">
                                
                                    <div>
                                        <img src="<?php echo $portafolio['portafolios_imagen_thumb']; ?>" alt=""/>
                                    </div>
                              
                            </figure>
                            </a>
                        </li>
            <?php 
                $i++;
            endforeach;
            ?>
                </ul>
            </div>
            <?php 
         endif;
        ?>
    </div> 
</div><!-- End Portfolio Section -->