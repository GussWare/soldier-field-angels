<?php
/**
 * Template Name: Nuestro Equipo
 * Description: Equipos
 */

get_header(); 
?>

<?php 
    if ( have_posts() ) : 
    ?>
        <!-- Start Page Title Section -->
        <div class="page-ttl slider-dos" style="">
            <div class="layer-stretch">
                <div class="page-ttl-container">
                    <h1>
                    <?php  
                        $titulo = get_field('titulo'); 
                        if($titulo):
                            echo $titulo;
                        endif;
                    ?>
                    </h1>
                </div>
            </div>
        </div><!-- End Page Title Section -->
    <?php

    ?>

        <div class="blog">
            <div class="layer-stretch">
                <div class="layer-wrapper pb-3">
                    <div class="list-container pt-4">

                    <div class="row">
                    
                    <?php 

                        $args = array(
                            'post_type' => 'cpt_equipo',
                            'orderby' => 'post_date',
                            'order' => 'ASC',
                        );
                        $wp_query = new WP_Query( $args );
                        
                        if($wp_query->have_posts()) :
                            while($wp_query->have_posts()): $wp_query->the_post();
                    ?>
                                <div class="col-sm-6 col-md-6 col-lg-3">
                                    <div class="team-block">
                                        <div class="team-img">
                                            <img src="<?php echo get_field('fotografia', get_the_ID()); ?>" alt="">
                                            <div class="team-description">
                                                <?php echo get_field('descripcion', get_the_ID()); ?>
                                            </div>
                                        </div>
                                        <div class="team-details">
                                            <h3><?php echo get_field('nombre_integrante', get_the_ID()); ?></h3>
                                            <p><?php echo get_field('puesto', get_the_ID()); ?></p>
                                        </div>
                                        <div class="team-social">
                                            <?php 
                                            $redes = get_field('redes_sociales', get_the_ID());
                                            if($redes):
                                            ?>
                                                <ul>
                                                    <?php foreach($redes as $red): ?>
                                                        <li><a href="<?php echo $red['link']; ?>"><i class="<?php echo $red['icono']; ?>"></i></a></li>
                                                    <?php endforeach; ?>
                                                </ul>
                                            <?php 
                                            endif;
                                            ?>
                                        </div>
                                    </div>
                                </div>
                    <?php  
                        endwhile; wp_reset_query();
                    endif;
                    ?>
                    </div>
                    </div>
                </div>
            </div>
        </div>

        <?php

    else: 

            get_template_part( 'message', 'none' );    

     endif; ?>

<?php get_footer(); ?>