<?php
/**
 * The main template file.
 *
 *
 */

get_header(); 
?>

    <?php 
    if ( have_posts() ) : 
    ?>
        <!-- Start Page Title Section -->
        <div class="page-ttl slider-dos">
            <div class="layer-stretch">
                <div class="page-ttl-container">
                    <h1>PRENSA</h1>
                </div>
            </div>
        </div><!-- End Page Title Section -->
    <?php

    ?>

        <div class="blog">
            <div class="layer-stretch">
                <div class="layer-wrapper pb-3">
                    <div class="list-container pt-4">
                    
                        <div class="row">

                            <?php

                            while(have_posts()): the_post();

                                get_template_part( 'content', get_post_format() );

                            endwhile;

                            ?>

                        </div>

                        <?php

                        // Previous/next page navigation.
                        the_posts_pagination( array(
                            'prev_text'          => "Previous page",
                            'next_text'          => "Next page",
                            'before_page_number' => '<span class="meta-nav screen-reader-text">Page</span>',
                        ) );
                        
                        ?>

                    </div>
                </div>
            </div>
        </div>

        <?php

    else: 

            get_template_part( 'message', 'none' );    

     endif; ?>

<?php get_footer(); ?>