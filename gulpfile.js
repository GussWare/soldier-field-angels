var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var sourcemaps = require('gulp-sourcemaps');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var replace = require('gulp-replace');

var dir_scss = './wp-content/themes/sfasoldiers/assets/sass/';
var dir_css = './wp-content/themes/sfasoldiers/';
var dir_js = './wp-content/themes/sfasoldiers/assets/js/';

gulp.task('sass', function () {
    gulp.src(dir_scss + '*.scss')
        .pipe(sass())
        .pipe(autoprefixer())
        .pipe(gulp.dest(dir_css))
});

gulp.task('default', ['sass']);